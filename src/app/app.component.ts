import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'basicApp';
  movieData ;
  movies = [];
  filteredData ;
  p = 1;
  count = 4;
  datalength = 0;
  isFilterData = false;

  constructor(private http:HttpClient ) {}

  ngOnInit() {
  }

 searchMovie(searchText) {
   this.isFilterData = false ;
   this.p = 1;
    this.http.get('http://www.omdbapi.com/?apikey=d19842a1&s=' + searchText)
    .subscribe(data => {
      this.movieData = data;
      this.movies = this.movieData.Search;
      this.datalength = this.movies.length;
    })
 } 

 sortData(event : any) {
   this.p = 1 ;
   const filterBy = event.target.value ;
   if(filterBy == 0) {
    if(!this.isFilterData) {
      this.movies = this.movies
    } 
    else {
      this.filteredData = this.filteredData ;
    }
   }
   else if(filterBy == 1) {
   if(!this.isFilterData) {
    this.movies.sort(function(a, b){
      var nameA=a.Title.toLowerCase(), nameB=b.Title.toLowerCase()
      if (nameA < nameB) //sort string ascending
          return -1 
      if (nameA > nameB)
          return 1
      return 0 //default return value (no sorting)
  })
   }
   else {
    this.filteredData.sort(function(a, b){
      var nameA=a.Title.toLowerCase(), nameB=b.Title.toLowerCase()
      if (nameA < nameB) //sort string ascending
          return -1 
      if (nameA > nameB)
          return 1
      return 0 //default return value (no sorting)
  })
   }
  }
  else if(filterBy == 2) {
  if(!this.isFilterData) {
    this.movies.sort(function(a, b){
      var nameA=a.Title.toLowerCase(), nameB=b.Title.toLowerCase()
      if (nameA < nameB) //sort string descending
          return 1 
      if (nameA > nameB)
          return -1
      return 0 //default return value (no sorting)
  })
  }
  else {
    this.filteredData.sort(function(a, b){
      var nameA=a.Title.toLowerCase(), nameB=b.Title.toLowerCase()
      if (nameA < nameB) //sort string descending
          return 1 
      if (nameA > nameB)
          return -1
      return 0 //default return value (no sorting)
  })
  }
  }
  
 }
 
 filterData(event : any) {
   this.p = 1;
   this.filteredData = [];
   if(event.target.value == 0) {
    this.filteredData = this.movies ; 
   }
   else if(event.target.value == 1) {
    this.isFilterData = true ;
     this.movies.forEach(element => {
      if(element.Type == 'movie') {
        this.filteredData.push(element);
      }
     });
     this.datalength = this.filteredData.length
   }
   else if(event.target.value == 2) {
    this.isFilterData = true ;
    this.movies.forEach(element => {
      if(element.Type == 'series') {
        this.filteredData.push(element);
      }
     });
     this.datalength = this.filteredData.length
   }
   else if(event.target.value == 3) {
    this.isFilterData = true ;
    this.movies.forEach(element => {
      if(element.Type == 'game') {
        this.filteredData.push(element);
      }
     });
     this.datalength = this.filteredData.length
   }
 }
 
}
